# About

Convert csv data created with the [discordChatExporter](https://github.com/Tyrrrz/DiscordChatExporter) by [Tyrrrz](https://github.com/Tyrrrz) on GitHub to the Slack CSV format.

# How to Use

0. (optional) Install [python3](https://www.python.org/downloads/) if you don't have it already.

1. Download the latest [discordChatExporter](https://github.com/Tyrrrz/DiscordChatExporter)

2. Follow the steps as described by the discordChatExporter to access your data:

<img src="images/000.PNG" width=350>

3. Select all channels that you want to export

<img src="images/001.PNG" width=350>

4. Pick CSV format and hit **EXPORT**:

<img src="images/002.PNG" width=350>

3. Download the `discordToSlack.py` file from this repository

4. In this example I exported to .csv files to my Desktop, and also copied the _discordToSlack.py_ there:

<img src="images/003.PNG" width=300>

5. I'll then run this command:

Command Line (because I'm on Windows):
```batch
C: & cd %HOMEPATH% & cd Desktop & python3 ./discordToSlack.py ./discord_data/
```

If you're on Linux/OSX it'd probably look something like this:

```bash
cd ~/Desktop; python3 ./discordToSlack.py ./discord_data/
```

6. Tadaaa , all channels have been combined to a new `./converted/discord_data_converted.csv` file, which you can import to your slack server, as described on [this page](https://slack.com/intl/en-dk/help/articles/201748703-import-data-to-slack?eu_nc=1).